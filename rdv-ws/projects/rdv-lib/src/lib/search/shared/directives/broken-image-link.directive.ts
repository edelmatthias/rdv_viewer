/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Directive, ElementRef, Input} from '@angular/core';

/**
 * Tries to display specified images.
 * First loadable image wins.
 */
@Directive({
  selector: 'img[appBrokenImage]'
})
export class BrokenImageLinkDirective {
  @Input() appBrokenImage: string[];

  constructor(private el: ElementRef) {
    const img: HTMLImageElement = el.nativeElement;
    img.addEventListener('error', () => {
      // avoid endless loop ...
      const src = img.src;
      let pos = -1;
      // the list contains perhaps relative urls, but img.src is absolute!
      this.appBrokenImage.map((u, index) => {
        if (src.indexOf(u) >= 0) {
          pos = index;
        }
      });
      if (pos >= 0 && (pos + 1) < this.appBrokenImage.length) {
        img.src = this.appBrokenImage[pos + 1];
      }
    })
  }
}
