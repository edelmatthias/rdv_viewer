/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendSearchService} from '../../shared/services/backend-search.service';
import {Location} from "@angular/common";
import {deepJsonClone, empty} from "../../shared/utils";
import {BehaviorSubject} from "rxjs";
import * as fromQueryActions from "../actions/query.actions";
import * as fromSearch from "../reducers/index";
import * as fromDetailedResultActions from "../actions/detailed-result.actions";
import {select, Store} from "@ngrx/store";
import {Observable, Subscription} from "rxjs";
import {take} from "rxjs/operators";
import {LocalizeRouterService} from "@gilsdav/ngx-translate-router";
import {environment} from "../../shared/Environment";
import {TranslateService} from "@ngx-translate/core";
import {BackendViewerType, ListType, Page, SettingsModel, ViewerType} from "../../shared/models/settings.model";
import {AutoCompleteValue, Doc, DocField, EditDoc, EditDocField, GroupInfo} from "../../shared/models/doc-details.model";
import {ResetUtilService} from "../../shared/services/reset-util.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Actions, ofType} from "@ngrx/effects";
import {I18nToastrService} from "../../shared/services/i18n-toastr.service";
import {availableIiifViewers, checkViewersFromBackend} from "../../shared/models/settings-util";


interface Info {
  detailedViewIds: any;
  meta: any;
  resultDisplayMode: any;
}

interface GroupDef {
  name: string;
  fields: DocField[];
}

/**
 * Shows detailed information on a document
 */
@Component({
  selector: 'app-detailed',
  template: `
    <ng-template #iiifViewerTemplate>
      <ng-container *ngIf="!doctype">
        <app-universal-viewer
          *ngIf="displayInUV(resultDisplayMode$ | async)"
          [manifestUrl]="manifestUrl" version="{{resultDisplayMode$ | async}}"></app-universal-viewer>
        <app-mirador
          *ngIf="displayInMirador(resultDisplayMode$ | async)"
          [manifestUrl]="manifestUrl" version="{{resultDisplayMode$ | async}}"></app-mirador>
      </ng-container>
    </ng-template>
    <div>
      <app-top></app-top>
      <div class="detailed" [class.detailed--doc-and-viewer]="horizEmbedIiifViewer()">
        <nav class="detailed-nav-toolbar d-flex">
          <div class="mr-auto">
            <a class="back-to-search link-black" (click)="backToSearch()">{{'detailed.search_result' | translate}}</a>
          </div>
          <div class="nav">
            <button *ngIf="!!cursor" type="button" class="previous-doc btn btn-link link-black" (click)="showPreviousDoc()"></button>
            <button *ngIf="!!cursor" type="button" class="next-doc btn btn-link link-black" (click)="showNextDoc()"></button>
            <app-doc-view-mode *ngIf="(detailedViewIds$ |async) && (meta$ | async)"
                               [disableListView]="horizEmbedIiifViewer() || vertEmbedIiifViewer()"
                               [resultDisplayModeSubject]="this.resultDisplayModeSubject"
                               [allowedViewers]="allIiifViewers()"></app-doc-view-mode>
          </div>
        </nav>
        <div *ngIf="showNarrowEmbeddedIiifViewer() && topIiifViewer()" class="mb-4">
          <ng-container [ngTemplateOutlet]="iiifViewerTemplate"></ng-container>
        </div>
        <ng-container *ngIf="showFormOnly(editing$ | async)">
          <div class="d-sm-flex no-gutters flex-sm-row-reverse">
            <div *ngIf="env.editable && !editingSubject.value" class="text-right flex-shrink-0 mt-sm-4">
              <button class="btn btn-red detailed__edit"
                      [disabled]="!docHasEditableFields"
                      (click)="editDoc()">{{'detailed.edit' | translate}}
              </button>
            </div>
            <div class="flex-grow-1">
              <ng-container *ngIf="!empty((meta$ | async).object_type)">
                <div class="search-hit__type">
                  <app-search-hit-value [fieldValue]="(meta$ | async).object_type"></app-search-hit-value>
                </div>
              </ng-container>
              <div class="row no-gutters">
                <h3 *ngIf="!empty((meta$ | async).title)" class="detailed__title col">
                  <app-search-hit-value [fieldValue]="(meta$ | async).title"></app-search-hit-value>
                </h3>
              </div>
              <ng-container *ngIf="doctype">
                <div class="search-hit__type">
                  <app-search-hit-value [fieldValue]="doctypeLabel()"></app-search-hit-value>
                </div>
                <div class="row no-gutters">
                  <h3 class="detailed__title col">
                    <app-search-hit-value [fieldValue]="'detailed.new' | translate"></app-search-hit-value>
                  </h3>
                </div>
              </ng-container>
            </div>
          </div>
          <div>
            <app-basket-icon *ngIf="env?.showExportList?.basket === true" [basketElement]="(meta$ | async).id"></app-basket-icon>
          </div>
          <form [formGroup]="formGroup" (ngSubmit)="saveDoc()" (keydown.enter)="handleEnterKeyPress($event)">
            <div [class.detailed__form--wide]="horizEmbedIiifViewer()">
              <ng-container *ngIf="leftIiifViewer()">
                <div class="detailed__embedded-viewer detailed__embedded-viewer--left">
                  <ng-container [ngTemplateOutlet]="iiifViewerTemplate"></ng-container>
                </div>
              </ng-container>
              <div class="detailed__fields" [class.detailed__fields--wide]="horizEmbedIiifViewer()">
                <ul class="no-list-style">
                  <li *ngFor="let field of (fields$ | async)">
                    <app-detailed-field *ngIf="!editField(field)"
                                        [fieldId]="field.field_id"
                                        [label]="field.label"
                                        [value]="field.value"
                                        [meta]="field.edit_service"></app-detailed-field>
                    <app-edit-field *ngIf="editField(field)"
                                    objectId="{{id}}"
                                    fieldId="{{field.field_id}}"
                                    [label]="field.label"
                                    [value]="editedFields[field.field_id]"
                                    [formGroup]="formGroup"
                                    [meta]="field.edit_service"></app-edit-field>
                  </li>
                  <li *ngFor="let fieldGroup of (fieldGroups$ | async)">
                    <app-collapsible [title]="fieldGroup.name | translate"
                                     [open]="(openGroups$ | async)[fieldGroup.name]"
                                     [key]="'fg-' + fieldGroup.name"
                                     (changed)="toggleGroupOpen(fieldGroup.name, $event)"
                    >
                      <ul class="no-list-style">
                        <li *ngFor="let field of fieldGroup.fields">
                          <app-detailed-field *ngIf="!editField(field)"
                                              [fieldId]="field.field_id"
                                              [label]="field.label"
                                              [value]="field.value"
                                              [meta]="field.edit_service"></app-detailed-field>
                          <app-edit-field *ngIf="editField(field)"
                                          objectId="{{id}}"
                                          fieldId="{{field.field_id}}"
                                          [label]="field.label"
                                          [value]="editedFields[field.field_id]"
                                          [formGroup]="formGroup"
                                          [meta]="field.edit_service"></app-edit-field>
                        </li>
                      </ul>
                    </app-collapsible>
                </ul>
                <div *ngIf="editing$ | async">
                  <button
                    class="btn btn-red detailed__edit"
                    type="submit"
                    [disabled]="!formGroup.valid"
                  >
                    {{'detailed.edit_save' | translate}}
                  </button>
                  <button class="btn btn-red detailed__edit" (click)="cancelEdit()">{{'detailed.edit_cancel' | translate}}</button>
                </div>
              </div>
              <ng-container *ngIf="rightIiifViewer()">
                <div class="detailed__embedded-viewer detailed__embedded-viewer--right">
                  <ng-container [ngTemplateOutlet]="iiifViewerTemplate"></ng-container>
                </div>
              </ng-container>
            </div>
          </form>
        </ng-container>
        <ng-container *ngIf="showIiifViewerOnly() || (showNarrowEmbeddedIiifViewer() && bottomIiifViewer())">
          <ng-container [ngTemplateOutlet]="iiifViewerTemplate"></ng-container>
        </ng-container>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailedComponent implements OnInit, OnDestroy, AfterViewInit {
  resultDisplayModeSubject = new BehaviorSubject<string>(ListType.LIST.toString());
  resultDisplayMode$: Observable<string>;
  viewersFromBackend: BackendViewerType = "ALL";

  /**
   * Identifier of document
   */
  id: any;

  // create doc of this type
  doctype: string;

  /**
   * Is document being edited?
   */
  editingSubject = new BehaviorSubject<boolean>(false);
  editing$: Observable<boolean>;
  editedFields?: EditDoc;
  docHasEditableFields?: boolean;
  saveErrorSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  createSuccessSubscription: Subscription;

  /**
   * If called from a search result list, it is possible to navigate to the "left" or "right" neighbour doc.
   */
  cursor?: any;

  manifestUrl: string;

  /**
   * Object with document information
   */
  meta$ = new BehaviorSubject<any>(undefined);
  fields$ = new BehaviorSubject<any>(undefined);
  fieldGroups$ = new BehaviorSubject<any>(undefined);
  openGroups: { [key: string]: boolean } = {};
  openGroups$ = new BehaviorSubject<any>({});
  detailedView$: Observable<any>;
  detailedViewIds$: Observable<any>;

  detailedViewSubscription: Subscription;
  languageSubscription: Subscription;

  location: Location;
  translate: TranslateService;

  parentRoute: ActivatedRoute;

  formGroup: FormGroup = new FormGroup({});

  /**
   * @ignore
   */
  constructor(private _backendSearchService: BackendSearchService,
              private _route: ActivatedRoute,
              protected router: Router,
              private _location: Location,
              private _searchStore: Store<fromSearch.State>,
              protected localize: LocalizeRouterService,
              protected activeRoute: ActivatedRoute,
              protected _translate: TranslateService,
              protected resetUtilService: ResetUtilService,
              protected actions$: Actions,
              protected toastr: I18nToastrService) {
    this.location = _location;
    const state: any = _location.getState();
    this.cursor = state.cursor;
    this.editing$ = this.editingSubject.asObservable();
    this.detailedView$ = _searchStore.pipe(select(fromSearch.getAllDetailedResults));
    this.detailedViewIds$ = _searchStore.pipe(select(fromSearch.getDetailedResultsIds));
    this.translate = _translate;
    this.languageSubscription = this.translate.onLangChange.subscribe((l) => {
      this.showCurrentDoc();
    });
    let vm;
    this._searchStore.select(fromSearch.getUsedResultViewer).pipe(take(1)).subscribe((v) => vm = v);
    this.resultDisplayModeSubject.next(vm);
    this.resultDisplayMode$ = this.resultDisplayModeSubject.asObservable();
    this.detailedViewSubscription = this.detailedViewIds$.subscribe((res) => this.detailedDataSubscriber(res));
    this.saveErrorSubscription = actions$
      .pipe(ofType(fromQueryActions.QueryActionTypes.DetailedSaveFailure))
      .subscribe((error) => this.showSaveErrorToast(error));
    this.saveSuccessSubscription = actions$
      .pipe(ofType(fromQueryActions.QueryActionTypes.DetailedSaveSuccess))
      .subscribe(() => this.finishEditing());
    this.createSuccessSubscription = actions$
      .pipe(ofType(fromQueryActions.QueryActionTypes.DetailedNewSuccess))
      .subscribe((res: any) => this.createEditing(res.payload));
    this.ensureConsistentIiifViewer();
  }

  // in embedded iiif viewer mode enable first iiif viewer automatically
  protected ensureConsistentIiifViewer() {
    const allIiifViewers = this.allIiifViewers();
    const currentView = this.currentView();
    if (this.horizEmbedIiifViewer() || this.vertEmbedIiifViewer()) {
      // allIiifViewers doesn't include list view
      if (allIiifViewers.length > 0) {
        if (!this.allIiifViewers().includes(currentView)) {
          this.resultDisplayModeSubject.next(allIiifViewers[0]);
        }
      } else {
        this.resultDisplayModeSubject.next(ListType.LIST.toString());
      }
    } else {
      // non embedded viewer mode
      if (currentView !== ListType.LIST.toString() && !this.allIiifViewers().includes(currentView)) {
        this.resultDisplayModeSubject.next(ListType.LIST.toString());
      }
    }
  }

  allIiifViewers(): string [] {
    return availableIiifViewers(Page.Detail, this.viewersFromBackend);
  }

  editField(field: DocField): boolean {
    return field.edit_service && this.editedFields && field.edit_service.edit && this.editingSubject.getValue();
  }

  handleEnterKeyPress(event) {
    const tagName = event.target.tagName.toLowerCase();
    if (tagName !== 'textarea') {
      event.preventDefault();
      return false;
    }
  }

  protected info(): Info {
    let detailedViewIds, meta, resultDisplayMode;
    if (this.detailedViewIds$) {
      this.detailedViewIds$.pipe(take(1)).subscribe(x => detailedViewIds = x);
    }
    if (this.meta$) {
      this.meta$.pipe(take(1)).subscribe(x => meta = x);
    }
    if (this.resultDisplayMode$) {
      this.resultDisplayMode$.pipe(take(1)).subscribe(x => resultDisplayMode = x);
    }
    return {detailedViewIds, meta, resultDisplayMode};
  }

  horizEmbedIiifViewer(): boolean {
    return !this.doctype && (environment.embeddedIiifViewer === 'left' || environment.embeddedIiifViewer === 'right');
  }

  vertEmbedIiifViewer(): boolean {
    return !this.doctype && (environment.narrowEmbeddedIiiFViewer === 'top' || environment.narrowEmbeddedIiiFViewer === 'bottom');
  }

  showIiifViewerOnly(): boolean {
    if (this.doctype || this.horizEmbedIiifViewer() || this.vertEmbedIiifViewer()) {
      return false;
    }
    const {detailedViewIds, meta, resultDisplayMode} = this.info();
    return detailedViewIds && meta && this.resultInViewer(resultDisplayMode);
  }

  rightIiifViewer(): boolean {
    return this.showEmbeddedIiifViewer() && environment.embeddedIiifViewer === 'right';
  }

  leftIiifViewer(): boolean {
    return this.showEmbeddedIiifViewer() && environment.embeddedIiifViewer === 'left';
  }

  bottomIiifViewer(): boolean {
    return this.showNarrowEmbeddedIiifViewer() && environment.narrowEmbeddedIiiFViewer === 'bottom';
  }

  topIiifViewer(): boolean {
    return this.showNarrowEmbeddedIiifViewer() && environment.narrowEmbeddedIiiFViewer === 'top';
  }

  showEmbeddedIiifViewer(): boolean {
    if (!this.horizEmbedIiifViewer()) {
      return false;
    }
    const {detailedViewIds, meta, resultDisplayMode} = this.info();
    return detailedViewIds && meta && !this.resultAsList(resultDisplayMode);
  }

  showNarrowEmbeddedIiifViewer(): boolean {
    if (!this.vertEmbedIiifViewer()) {
      return false;
    }
    const {detailedViewIds, meta, resultDisplayMode} = this.info();
    return detailedViewIds && meta && !this.resultAsList(resultDisplayMode);
  }

  showFormOnly(editing: boolean): boolean {
    const {detailedViewIds, meta, resultDisplayMode} = this.info();
    const viewEditMode = detailedViewIds
      && meta
      && (this.horizEmbedIiifViewer() || this.vertEmbedIiifViewer() || this.resultAsList(resultDisplayMode));
    const newMode = this.doctype && editing;
    return viewEditMode || newMode;
  }

  showSaveErrorToast(error: any) {
    this.toastr.error('detailed.save_error_message', 'detailed.save_title');
  }

  showSaveOkToast() {
    this.toastr.info('detailed.save_ok_message', 'detailed.save_title');
  }

  finishEditing() {
    this.showSaveOkToast();
    this.cancelEdit();
    this.showCurrentDoc(); // reload new document data
  }

  // write {{invalidInfos() | json}} into the template to view form's validation state in detail
  invalidInfos() {
    const result = {};
    Object.keys(this.formGroup.controls).map(key => {
      result[key] = this.formGroup.controls[key].invalid;
    });
    return result;
  }

  /**
   * Retrieves document data from backend
   */
  ngOnInit() {
    this._route.paramMap.pipe(take(1)).subscribe(params => {
      this.id = params.get('id');
      this.doctype = params.get("type");
      this.showCurrentDoc();
    });
    this.parentRoute = this.activeRoute.parent;
  }

  protected showCurrentDoc() {
    this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
    if (this.doctype) {
      this._searchStore.dispatch(new fromQueryActions.MakeDetailedNewRequest({doctype: this.doctype}));
    } else {
      if (this.editingSubject.getValue()) {
        this._searchStore.dispatch(new fromQueryActions.MakeDetailedEditRequest({id: this.id, fullRecord: false}));
      } else {
        this._searchStore.dispatch(new fromQueryActions.MakeDetailedSearchRequest({id: this.id, fullRecord: false}));
      }
    }
  }

  empty(obj: any): boolean {
    return empty(obj);
  }

  showPreviousDoc() {
    if (this.leaveEditMode()) {
      this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
      this._searchStore.dispatch(new fromQueryActions.MakePreviousDetailedSearchRequest(this.cursor));
    }
  }

  showNextDoc() {
    if (this.leaveEditMode()) {
      this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
      this._searchStore.dispatch(new fromQueryActions.MakeNextDetailedSearchRequest(this.cursor));
    }
  }

  backToSearch() {
    if (this.leaveEditMode()) {
      // only list view mode allows to switch to detail view ... so switch back if changed
      this.switchToListMode(); // keep it in the case of a deep link

      // if detail view was first page
      if (!this.cursor) {
        this.resetUtilService.switchToSearchView().then(() => {
            // normally, search view starts empty search, but not if previous url was the detail view
            // so start search here
            this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
          }
        );
      } else {
        // this.location.back();
        this.router.navigate(['.'], {relativeTo: this.parentRoute});
      }
    }
  }

  private switchToListMode() {
    this._searchStore.dispatch(new fromQueryActions.SetResultListDisplayMode(ListType.LIST.toString()));
  }

  currentView(): string {
    let currentViewer;
    this.resultDisplayMode$.pipe(take(1)).subscribe((v) => currentViewer = v);
    return currentViewer;
  }

  detailedDataSubscriber(ids) {
    let detailedViews;
    this.docHasEditableFields = false;
    this.detailedView$.pipe(take(1)).subscribe((docs) => detailedViews = docs);
    if (ids && ids.length > 0 && detailedViews) {
      const newId = ids[ids.length - 1];
      const res = detailedViews[newId];
      if (res) {
        const funcMetadata = res.func_metadata;
        this.processFuncMetadata(funcMetadata, newId);

        const descMetadata: DocField[] = res.desc_metadata;
        this.fields$.next(this.getNonGroupedFields(descMetadata));
        const groupMetaData = funcMetadata ? funcMetadata.groups : undefined;
        this.fieldGroups$.next(this.getGroupedFields(descMetadata, groupMetaData));
        if (descMetadata && this.checkEditableDocFields(descMetadata)) {
          if (this.editingSubject.getValue()) {
            this.editedFields = this.initEditData(descMetadata);
          }
        } else {
          this.leaveEditMode();
        }
      }
    }
  }

  protected processFuncMetadata(funcMetadata, newId) {
    this.viewersFromBackend = "ALL";
    if (funcMetadata) {
      if (funcMetadata.viewer && funcMetadata.viewer.length > 0) {
        if (funcMetadata.viewer === ["None"]) {
          this.viewersFromBackend = "NONE";
        } else {
          this.viewersFromBackend = checkViewersFromBackend(Page.Detail, funcMetadata.viewer);
        }
        this.ensureConsistentIiifViewer();
      }
      if (funcMetadata.search_after_values && funcMetadata.search_after_values !== '[]') {
        if (funcMetadata.search_after_values !== "[]") {
          this.cursor = funcMetadata.search_after_values;
        }
        this.id = newId;
        const newUrl = this.localize.translateRoute('/detail/' + encodeURIComponent(newId));
        // don't push new url to browser's history, so that browser back returns to search view
        this.router.navigate([newUrl], {replaceUrl: true});
      }
      this.meta$.next(funcMetadata);
      if (funcMetadata.iiif_manifest) {
        this.manifestUrl = funcMetadata.iiif_manifest;
      }
    }
  }

  getNonGroupedFields(desc_metadata: DocField[]): any[] {
    return desc_metadata.filter(dm => !dm.group);
  }

  getGroupedFields(desc_metadata: DocField[], groupMetaData: { [key: string]: GroupInfo }): GroupDef[] {
    const groupDefs: { [key: string]: GroupDef } = {};
    desc_metadata.map(df => {
      if (df.group) {
        let g = groupDefs[df.group];
        if (!g) {
          g = groupDefs[df.group] = {name: df.group, fields: []};
        }
        g.fields.push(df);
      }
    });
    const groups: GroupDef[] = [];
    Object.keys(groupDefs).forEach((key) => {
      groups.push(groupDefs[key]);
    });
    if (groupMetaData) {
      groups.sort((gd1, gd2) => groupMetaData[gd1.name].order - groupMetaData[gd2.name].order);
      let openState = {...this.openGroups};
      groups.forEach(gd => {
        const open = groupMetaData[gd.name].open === true;
        openState = this.setOpenState(openState, gd.name, open);
      });
      this.openGroups = openState;
      this.openGroups$.next(openState);
    }
    return groups;
  }

  setOpenState(openState: { [key: string]: boolean }, groupName: string, open: boolean): { [key: string]: boolean } {
    if (!open) {
      delete openState[groupName];
    } else {
      openState[groupName] = true;
    }
    return openState;
  }

  toggleGroupOpen(groupName: string, open: boolean) {
    this.openGroups = this.setOpenState({...this.openGroups}, groupName, open);
    this.openGroups$.next(this.openGroups);
  }

  checkEditableDocFields(desc_metadata: DocField[]): boolean {
    const count = desc_metadata.filter(f => f.edit_service.edit).length;
    this.docHasEditableFields = count > 0;
    return this.docHasEditableFields;
  }

  // converts /object_edit desc_metadata response to EditDoc
  protected initEditData(desc_metadata): EditDoc {
    const currentLang = this.translate.currentLang;
    const defaultLang = environment.defaultLanguage || "de";
    const result: EditDoc = {};
    this.formGroup = new FormGroup({});

    deepJsonClone(desc_metadata)
      .filter(f => f.edit_service && f.edit_service.edit)
      .map(f => {
        // console.log("ORIG", JSON.stringify(f));
        // fields without values are represented by an empty array (instead of empty object)
        if (Array.isArray(f.value) && f.value.length === 0) {
          f.values = [];
        } else if (typeof f.value === "object" && !Array.isArray(f.value)) {
          const fieldValues = f.value[currentLang] ? f.value[currentLang] : f.value[defaultLang];
          f.values = fieldValues.map(v => {
            if (typeof v === 'object' && !Array.isArray(v)) {
              if (!v.value) {
                v.value = {};
              }
              v.value.id = v.id ? v.id : v.label;
              if (Array.isArray(v.value.id)) {
                v.value.id = v.value.id.join("|||");
              }
              if (Array.isArray(v.label)) {
                v.label = v.label.join(",");
              }
              if (!v.label) {
                v.label = v.value.id;
              }
              v.id = v.value.id;
              return v;
            }
            if (Array.isArray(v)) {
              const id = v.join("|||");
              const label = v.join(",");
              return {id: id, label: label, value: {id: id}, service_label: label};
            }
            const vs = "" + v;
            return {id: vs, label: vs, value: {id: v}, service_label: vs};
          });
        }

        const validators = [];
        if (f.edit_service.required) {
          validators.push(Validators.required);
        }

        // prepare field values ...
        let initialValue = f.edit_service.repeatable ? [] : undefined;
        if (f.values) {
          const allValues = f.values.map(v => v.value.id);
          if (f.edit_service.repeatable) {
            initialValue = allValues;
          } else if (allValues.length > 0) {
            initialValue = allValues[0];
          }
        }
        this.formGroup.addControl(f.field_id, new FormControl(initialValue, validators));

        delete f.value; // replaced with "values"
        delete f.label; // field label
        // console.log("MAPPED", JSON.stringify(f));
        result[f.field_id] = f;
      });
    return result;
  }

  ngOnDestroy(): void {
    if (this.detailedViewSubscription) {
      this.detailedViewSubscription.unsubscribe();
    }
    if (this.languageSubscription) {
      this.languageSubscription.unsubscribe();
    }
    if (this.saveErrorSubscription) {
      this.saveErrorSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.createSuccessSubscription) {
      this.createSuccessSubscription.unsubscribe();
    }
  }

  get env(): SettingsModel {
    return environment;
  }

  resultInViewer(resultDisplayMode): boolean {
    return !this.doctype && resultDisplayMode !== ListType.LIST.toString();
  }

  resultAsList(resultDisplayMode): boolean {
    return resultDisplayMode === ListType.LIST.toString();
  }

  displayInUV(resultDisplayMode): boolean {
    return !this.doctype && environment.documentViewer[resultDisplayMode].type === ViewerType.UV;
  }

  displayInMirador(resultDisplayMode): boolean {
    return !this.doctype && environment.documentViewer[resultDisplayMode].type === ViewerType.MIRADOR;
  }

  enterEditMode() {
    this.editingSubject.next(true);
  }

  leaveEditMode(): boolean {
    if (this.editingSubject.getValue()
      && (this.formGroup.dirty || this.formGroup.touched)
      && !window.confirm(this.translate.instant('detailed.edit_loose_changes_hint'))) {
      return false;
    }
    this.editingSubject.next(false);
    return true;
  }

  editDoc() {
    this.enterEditMode();
    if (!this.horizEmbedIiifViewer() && !this.vertEmbedIiifViewer()) {
      this.resultDisplayModeSubject.next(ListType.LIST.toString());
    }
    this._searchStore.dispatch(new fromDetailedResultActions.ClearDetailedResults());
    this._searchStore.dispatch(new fromQueryActions.MakeDetailedEditRequest({id: this.id, fullRecord: false}));
  }

  cancelEdit() {
    if (this.doctype) {
      this.backToSearch();
    } else {
      this.formGroup.reset();
      this.leaveEditMode();
      delete this.editedFields;
      this.resetUtilService.scrollToTop();
    }
  }

  saveDoc() {
    const result: EditDocField[] = [];
    Object.keys(this.editedFields).map(key => {
      const field: EditDocField = deepJsonClone(this.editedFields[key]);
      field.values.map((item: AutoCompleteValue) => {
        delete item.service_label;
        delete item.id;
        delete item.selection;
      });
      result.push(field);
    });

    this._searchStore.dispatch(new fromQueryActions.MakeSaveDetailedEditRequest({
      object_id: this.id,
      fields: result,
      objectType: this.doctype
    }));
    // MakeSaveDetailedEditRequest's result state is handled by
    // this.saveSuccessSubscription and this.saveErrorSubscription
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.resetUtilService.scrollToTop(), 10);
  }

  createEditing(res: Doc) {
    const funcMetadata = res.func_metadata;
    const descMetadata: DocField[] = res.desc_metadata;
    this.viewersFromBackend = "NONE";
    this.fields$.next(this.getNonGroupedFields(descMetadata));
    const groupMetaData = funcMetadata ? funcMetadata.groups : undefined;
    this.fieldGroups$.next(this.getGroupedFields(descMetadata, groupMetaData));
    if (descMetadata && this.checkEditableDocFields(descMetadata)) {
      this.editedFields = this.initEditData(descMetadata);
    }
    this.meta$.next({});
    this.enterEditMode();
  }

  doctypeLabel() {
    let label = "UNKNOWN";
    const creatable = environment.creatable;
    for (let i = 0; i < creatable.length; i++) {
      if (creatable[i].value === this.doctype) {
        label = "" + creatable[i].label[this.translate.currentLang];
        break;
      }
    }
    return label;
  }
}
