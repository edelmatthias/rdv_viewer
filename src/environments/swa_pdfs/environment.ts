import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  SettingsModel,
  SortOrder
} from "rdv-lib";

import {environment as swa} from '@env/swa/environment';
import {environment as zas} from "@env/zas/environment";

export const environment: SettingsModel = {
  ...swa,
  editable: true,
  showImagePreview: false,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: true,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },
  facetFields: {
    //...zas.facetFields,
    "title2": {
      "field": "title2.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IZID Datensatz",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "Type": {
      "field": "type_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Kleinschrift Typ",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10
    },
    "delete": {
      "field": "delete",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Gelöscht",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
    },
    "sys_created": {
      "field": "sys_created",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "PDF Upload",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "sys_updated": {
      "field": "sys_updated",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Aktualisiert",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "tag": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 20,
    },
    "filename": {
      "field": "filename.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Dateiname",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "bsiz_id": {
      "field": "bsiz_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IZ ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "page_sysid": {
      "field": "page_sysid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Systemnummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 22,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    }
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "sys_created",
      order: SortOrder.ASC,
      display: "Upload (älteste)"
    },
    {
      field: "sys_created",
      order: SortOrder.DESC,
      display: "Upload (neueste)"
    }
  ],

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [20, 50, 100, 200],

  i18n: {
    "de": {
      "env.facetFields.fct_pubyear": "Erscheinungsjahr",
      "env.facetFields.fct_access": "Zugänglichkeit",
      "env.facetFields.fct_objecttype": "Objekttyp",
      "env.facetFields.fct_author": "Verfasser/in",
      "env.facetFields.descr_han_hierarchy_han_filter": "Bestandsgliederung",
      "env.facetFields.descr_han_hierarchy_per_filter": "Bestandsgliederung Nachlässe",
      "env.facetFields.descr_han_hierarchy_verband_filter": "Bestandsgliederung Verbände",
      "top.headerSettings.name": "SWA PDF Kleinschriften Upload",
      "top.headerSettings.name.Dev": "SWA PDF Kleinschriften Upload (Dev)",
      "top.headerSettings.name.Loc": "SWA PDF Kleinschriften Upload (Loc)",
      "top.headerSettings.name.Test": "SWA PDF Kleinschriften Upload (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
    "en": {
      "env.facetFields.fct_pubyear": "Year of publication",
      "env.facetFields.fct_access": "Accessibility",
      "env.facetFields.fct_objecttype": "Objekttyp",
      "env.facetFields.fct_author": "Author",
      "top.headerSettings.name": "SWA PDF Kleinschriften Upload",
      "top.headerSettings.name.Dev": "SWA PDF Kleinschriften Upload (Dev)",
      "top.headerSettings.name.Loc": "SWA PDF Kleinschriften Upload (Loc)",
      "top.headerSettings.name.Test": "SWA PDF Kleinschriften Upload (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    }
  }
}

initRdvLib(environment);
