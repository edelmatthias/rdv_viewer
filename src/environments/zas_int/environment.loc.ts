const Proj = "zas_int-loc";

import {initRdvLib, SettingsModel} from 'rdv-lib'
import {environment as proj} from "@env/zas_int/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...loc,
  headerSettings: addLocNamePostfix(proj.headerSettings),

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: loc.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: loc.detailSuggestionProxyUrl +  Proj + "/",
  detailEditProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_edit/" +  Proj + "/",
  /*detailNewProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "retrodigitalisierter Zeitungsausschnitt",
        "en": "scanned press clipping"
      },
      value: "dizas"
    },
    {
      label: {
        "de": "elektronischer Zeitungsausschnitt (ab 2013)",
        "en": "digitial press clipping (since 2013)"
      },
      value: "ezas"
    }
  ],*/

};

initRdvLib(environment);
